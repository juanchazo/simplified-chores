import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by juanchazo on 6/8/16.
 */
public class Person {
    private String name;
    private int chore_count;
    private HashMap<String, Integer> my_chores = new HashMap<>();

    public Person (String name) {
        this.name = name;
        this.chore_count = 0;
    }

    public void addChore(String chore, int day) {
        my_chores.put(chore, day);
    }

    public void removeChore(String chore) {
        if (my_chores.containsKey(chore)) my_chores.remove(chore);
        else throw new NullPointerException("This person does not have this chore.");
    }

    public int numOfChores() {
        return my_chores.size();
    }

    public HashMap<String, Integer> showChoresList() {
        HashMap<String, Integer> choresList = my_chores;
        return choresList;
    }

    public String returnName() {
        return this.name;
    }
    public void printChores() {
        System.out.println(my_chores.entrySet());
    }

    public void printName() {
        System.out.println(this.name);
    }
}
