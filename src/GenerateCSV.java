/**
 * Created by juanchazo on 6/10/16.
 */

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class GenerateCSV {
    public GenerateCSV(ArrayList<Person> housemates) {
        try {
            FileWriter writer = new FileWriter("monthly_chores.csv");

            writer.append("Subject");
            writer.append(',');
            writer.append("Start Date");
            writer.append(',');
            writer.append("Description");
            writer.append('\n');

            Calendar cal = new GregorianCalendar();

            for (Person p : housemates) {
                HashMap<String, Integer> temp = p.showChoresList();
                Object[] obj = temp.keySet().toArray();
                String[] chore_name = Arrays.asList(obj).toArray(new String[obj.length]);
                obj = temp.values().toArray();
                Integer[] chore_day = Arrays.asList(obj).toArray(new Integer[obj.length]);

                for (int i = 0; i < chore_name.length; i++) {
                    writer.append(p.returnName() + " - " + chore_name[i]);
                    writer.append(',');
                    writer.append(cal.get(Calendar.MONTH)+1 + "/" + chore_day[i].toString() + "/"
                            + cal.get(Calendar.YEAR));
                    writer.append(',');
                    writer.append(chore_name[i]);
                    writer.append("\n");
                }
            }
            writer.flush();
            writer.close();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }
}
