import java.io.*;
import java.util.*;

/**
 * Created by juanchazo on 6/8/16.
 */

public class ChoreScheduler {
    // Initialize array to store people.
    ArrayList<Person> housemates = new ArrayList<>();
    String[] days_of_week = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    // Method that calls the two helper methods to do everything.
    public ChoreScheduler(ArrayList<String> names, ArrayList<String> chores_list) {
        populate_housemates(names);
        monthly_chores(chores_list);
    }

    // Populates the housemates array list to initialize each Person with their respective names.
    private void populate_housemates(ArrayList<String> names) {
        if (names.isEmpty()) return;

        ArrayList<String> nameList = names;
        int num_of_persons = nameList.size();
        for (int i = 0; i < num_of_persons; i++) {
            housemates.add(new Person(nameList.get(i)));
        }
    }

    // Adds chores to each Person in the housemates array list.
    private void monthly_chores(ArrayList<String> chores_list) {
        if (chores_list.isEmpty()) return;

        // Initializes a calendar, gets the number of days in this month.
        Calendar calendar = Calendar.getInstance();
        int num_of_days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int buffer = num_of_days / chores_list.size();

        Collections.shuffle(housemates);
        Iterator<Person> iterator = housemates.iterator();

        ArrayList<String> remainingChores = chores_list;
        for (int day = 0; day < num_of_days; day+=buffer) {
            if (remainingChores.isEmpty()) break;

            if (!iterator.hasNext()) iterator = housemates.iterator();

            Person p = iterator.next();
            int chore = randInt(remainingChores.size());
            p.addChore(remainingChores.get(chore), day+1);
            remainingChores.remove(chore);
        }
    }

    private void check_balances(ArrayList<String> remaining_chores) {

    }

    private int randInt(int max) {
        Random rand = new Random();
        return rand.nextInt(max);
    }

    public static void main(String args[]) {
        ArrayList<String> chores = new ArrayList<>();
        ArrayList<String> names = new ArrayList<>();

        // Read in list of chores from chores file.
        try (BufferedReader br = new BufferedReader(new FileReader("chores.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!chores.contains(line)) chores.add(line);
            }
        }
        catch(FileNotFoundException fnf) {
            System.out.println("File not found");
        }
        catch (IOException e) {
            System.out.println("IO Exception");
        }

        // Read in list of names from names file.
        try (BufferedReader br = new BufferedReader(new FileReader("names.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (!names.contains(line)) names.add(line);
            }
        }
        catch(FileNotFoundException fnf) {
            System.out.println("File not found");
        }
        catch (IOException e) {
            System.out.println("IO Exception");
        }

        ChoreScheduler cs = new ChoreScheduler(names, chores);
        GenerateCSV g = new GenerateCSV(cs.housemates);
    }

}
